#include "Trainer.h"
#include <conio.h>
#include <string>
#include <iostream>

using namespace std;

Trainer::Trainer()
{
	this->name = "";
	this->gender;
	this->initialLocationX = 0;
	this->initialLocationY = 0;

}

Trainer::Trainer(string name, char gender, int initialLocationX, int initialLocationY)
{
	this->name = name;
	this->gender = gender;
	/*int initialLocationX = 0;
	int initialLocationY = 0;*/
	
	
}


void Trainer::encounter( vector<Pokemon*> wildPokemon, vector<Pokemon*> myPokemon)
{
	int answerBattle;
	int encounterChance = rand() % 100 + 1;

	if (encounterChance <= 45)
	{
		cout << "You have encountered a wild Pokemon!" << endl;

		int i = rand() % 16 + 1;

		cout << "A wild " << wildPokemon[i]->pokemonName << " has appeared." << endl;

		cout << "Type: " << wildPokemon[i]->type << endl;
		cout << "Level: " << wildPokemon[i]->initialLevel << endl;
		cout << "==================================================" << endl;

		cout << "What action do you want to do: " << endl;
		cout << "[7] Catch " << endl;
		cout << "[8] Battle " << endl;
		cout << "[9] Run Away" << endl;

		cout << "Answer: ";
		cin >> answerBattle;
		cout << endl;
		cout << "===================================================" << endl;

		if (answerBattle == 7)
		{
			cout << "You threw PokeBall." << endl;
 			int x = rand() % 100 + 1;

			if (x <= 30)
			{
				cout << "You caught wild Pokemon " << wildPokemon[i]->pokemonName << endl;
				myPokemon.push_back(wildPokemon[i]);

				_getch();
			}
			else
			{
				cout << "Pokemon got out of the ball. " << endl;
				return;
			}
		}
		 else if (answerBattle == 8)
		{
				int currentPokemon = 0;
				
				while (myPokemon[0]->initialHp >= 1)
				{
					cout << endl;
					cout << myPokemon[currentPokemon]->pokemonName << " attacks " << wildPokemon[i]->pokemonName;
					myPokemon[currentPokemon]->initialDamage -= wildPokemon[i]->initialHp;
					cout << wildPokemon[i]->pokemonName << " has a remaining HP of: " << wildPokemon[i]->initialHp << endl;

					cout << wildPokemon[i]->pokemonName << " attacks " << myPokemon[currentPokemon]->pokemonName << " ." << endl;
					myPokemon[0]->initialHp = wildPokemon[i]->initialDamage - myPokemon[1]->initialHp;
					cout << myPokemon[currentPokemon]->pokemonName << " has a remaining HP of: " << myPokemon[currentPokemon]->initialHp << endl;
				}
				cout << "Your Pokemon has fainted. " << endl;
				cout << " [R] Run Away or [T] Next Pokemon " << endl;
				char defeatOption;
				cout << "Answer: " << endl;
				cin >> defeatOption;
				cout << endl;
				if (defeatOption == 'R' || 'r')
				{
					cout << "You and your Pokemon fled. " << endl;
				}
				if (defeatOption == 'T' || 't')
				{
					currentPokemon += 1;
					cout << "You draw " << myPokemon[currentPokemon]->pokemonName << " to battle." << endl;
					return;
				}
			
		}
		 else if (answerBattle == 9)
		{
			cout << "You ran away." << endl;
			
		}
	}

	else
	{
		cout << "You have not encountered any Pokemon." << endl;

	}
	_getch();
	return;
}

void Trainer::pokemonAttack(vector<Pokemon*> wildPokemon, vector<Pokemon*> myPokemon)
{
	int currentPokemon = 0;
	int i = rand() % 16 + 1;

	while (myPokemon[currentPokemon]->initialHp != 0) 
	{
		cout << endl;
		cout << myPokemon[currentPokemon]->pokemonName << " attacks " << wildPokemon[i]->pokemonName << endl;
		myPokemon[currentPokemon]->initialDamage -= wildPokemon[i]->initialHp;
		cout << wildPokemon[i]->pokemonName << " has a remaining HP of: " << wildPokemon[i]->initialHp << endl;

		cout << wildPokemon[i]->pokemonName << " attacks " << myPokemon[currentPokemon]->pokemonName << " ." << endl;
		wildPokemon[i]->initialDamage -= myPokemon[0]->initialHp;
		cout << myPokemon[currentPokemon]->pokemonName << " has a remaining HP of: " << myPokemon[currentPokemon]->initialHp << endl;
		
	
	}
	
		cout << "Your Pokemon has fainted. " << endl;
		cout << " [R] Run Away or [T] Next Pokemon " << endl;
		char defeatOption;
		cout << "Answer: " << endl;
		cin >> defeatOption;
		cout << endl;
	
	if (defeatOption == 'R' || 'r')
	{
		cout << "You and your Pokemon fled. " << endl;
		while (false);
	}
	else if (defeatOption == 'T' || 't')
	{
		currentPokemon += 1;
		cout << "You draw " << myPokemon[currentPokemon]->pokemonName << " to battle." << endl;
		return;
	}

}


string Trainer::mapAndGame(int initialLocationX, int initialLocationY, vector<Pokemon*>wildPokemon, vector <Pokemon*> myPokemon)
{
	
	cout << "Let us start the adventure! Here are the instructions to move: " << endl;
	while (true)
	{
		int answerMovement;

		cout << "Press [1] to move Forward. " << endl;
		cout << "Press [2] to move Backward." << endl;
		cout << "Press [3] to move Left." << endl;
		cout << "Press [4] to move Right." << endl;
		cout << "Press [0] to display Pokemon " << endl;

		cout << "Answer: ";
		cin >> answerMovement;
		cout << endl;

		if (answerMovement == 1)
		{
			initialLocationY = initialLocationY + 1;
			cout << "( " << initialLocationX << " , " << initialLocationY << " )" << endl;
			cout << "You moved Forward. " << endl;
		}

		else if (answerMovement == 2)
		{
			initialLocationY = initialLocationY - 1;
			cout << "( " << initialLocationX << " , " << initialLocationY << " )" << endl;
			cout << "You moved Backward. " << endl;
		}
		else if (answerMovement == 3)
		{
			initialLocationX = initialLocationX - 1;
			cout << "( " << initialLocationX << " , " << initialLocationY << " )" << endl;
			cout << "You moved Left. " << endl;
		}
		else if (answerMovement == 4)
		{
			initialLocationX = initialLocationX + 1;
			cout << "( " << initialLocationX << " , " << initialLocationY << " )" << endl;
			cout << "You moved Right. " << endl;
		}
		else if (answerMovement == 0)
		{
			for (int i=0; i <= myPokemon.size(); i++)
			{
				cout << "Name: " << myPokemon.at(i)->pokemonName << endl;
				cout << "Type: " << myPokemon.at(i)->type << endl;
				cout << "Level " << myPokemon.at(i)->initialLevel << endl;
			}
		}

		cout << "=======================================================" << endl;

		if ((initialLocationX <= 1 && initialLocationX >= -1) || (initialLocationY <= 1 && initialLocationY >= -1))
		{
			cout << "Location: Little Root Town" << endl;

		}


		else if ((initialLocationX <= 4 && initialLocationX >= -4) || (initialLocationY <= 4 && initialLocationY >= -4))
		{
			
			cout << "Location: Wilderness" << endl;
			cout << "You may encounter Pokemon here! " << endl;

			int answerBattle;
			int encounterChance = rand() % 100 + 1;

			while (encounterChance <= 45)
			{
				cout << "You have encountered a wild Pokemon!" << endl;

				int i = rand() % 16 + 1;

				cout << "A wild " << wildPokemon[i]->pokemonName << " has appeared." << endl;

				cout << "Type: " << wildPokemon[i]->type << endl;
				cout << "Level: " << wildPokemon[i]->initialLevel << endl;
				cout << "==================================================" << endl;

				cout << "What action do you want to do: " << endl;
				cout << "[7] Catch " << endl;
				cout << "[8] Battle " << endl;
				cout << "[9] Run Away" << endl;

				cout << "Answer: ";
				cin >> answerBattle;
				cout << endl;
				cout << "===================================================" << endl;

				if (answerBattle == 7)
				{
					int tryAgain = 5;
					while (tryAgain == 5)
					{
						cout << "You threw PokeBall." << endl;
						int x = rand() % 100 + 1;

						if (x <= 30)
						{
							cout << "You caught wild Pokemon " << wildPokemon[i]->pokemonName << endl;
							myPokemon.push_back(wildPokemon[i]);

							_getch();
						}
						else
						{
							cout << "Pokemon got out of the ball. " << endl;

							cout << "[5] Try again or [6] Leave " << endl;
							cin >> tryAgain;
							cout << endl;
							if (tryAgain == 5)
							{

								true;
							}
						}
					}
					if (tryAgain == 6)
					{

						cout << "You Left. " << endl;

						break;
						
					}
					
					
				}

				else if (answerBattle == 8)
				{
					int currentPokemon = 0;
					int defeatOption;
					int gogoBattle = 1;

					while (gogoBattle == 1)
					{
						cout << endl;
						cout << myPokemon[currentPokemon]->pokemonName << " attacks " << wildPokemon[i]->pokemonName;
						myPokemon[currentPokemon]->initialDamage -= wildPokemon[i]->initialHp;
						cout << wildPokemon[i]->pokemonName << " has a remaining HP of: " << wildPokemon[i]->initialHp << endl;

						system("Pause");

						cout << wildPokemon[i]->pokemonName << " attacks " << myPokemon[currentPokemon]->pokemonName << " ." << endl;
						myPokemon[0]->initialHp = wildPokemon[i]->initialDamage - myPokemon[0]->initialHp;
						cout << myPokemon[currentPokemon]->pokemonName << " has a remaining HP of: " << myPokemon[currentPokemon]->initialHp << endl;

						system("Pause");


						if (myPokemon[0]->initialHp == 0)
						{
							cout << "Your Pokemon has fainted. " << endl;
							cout << " [R] Run Away or [T] Next Pokemon " << endl;


							cout << "Answer: " << endl;
							cin >> defeatOption;
							cout << endl;
							if (defeatOption == 1)
							{
								cout << "You and your Pokemon fled. " << endl;
								break;
							}
							else if (defeatOption == 2)
							{
								currentPokemon += 1;
								cout << "You draw " << myPokemon[currentPokemon]->pokemonName << endl;
								cout << "What action do you want to do: " << endl;
								cout << "[7] Catch " << endl;
								cout << "[8] Battle " << endl;
								cout << "[9] Run Away" << endl;

								cout << "Answer: ";
								cin >> answerBattle;
								cout << endl;
								cout << "===================================================" << endl;

								true;
			
							}


						}
					}
				}
				else if (answerBattle == 9)
				{
					cout << "You ran away." << endl;
					break;
				}


				
			}
			
			
				cout << "You have not encountered any Pokemon." << endl;

				_getch();
				
		}

		true;
	}
}

