#pragma once
#include <conio.h>
#include <string>
#include <iostream>
#include <vector>


using namespace std;

class Pokemon
{
public:


	Pokemon();
	Pokemon(string pokemonName, string type, int initialHp, int initialLevel, int initialDamage);
	string pokemonName;
	string type;
	int initialLevel;
	int initialHp;
	int initialDamage;
	int initialExp;
	int expToNextLevel = 100;
	vector<Pokemon*> wildPokemon;
	vector<Pokemon*> myPokemon;
	void display_pokemon(vector <Pokemon*> pokemon);
	;
};

